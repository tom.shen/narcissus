package mark.tom.narcissus.demo;

import android.content.Context;

import androidx.preference.PreferenceManager;

import java.util.Random;

import androidx.annotation.NonNull;
import mark.tom.narcissus.template.IComponents;

public class AComponents implements IComponents {

    @Override
    public void registerComponents(@NonNull Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("AComponents", "Data from AComponents -> " + new Random().nextInt(100))
                .apply();
    }
}
