package mark.tom.narcissus.demo;

import android.content.Context;
import android.graphics.Color;

import com.jraska.console.timber.ConsoleTree;

import androidx.annotation.NonNull;
import mark.tom.narcissus.template.IComponents;
import timber.log.Timber;

public class BComponents implements IComponents {

    @Override
    public void registerComponents(@NonNull Context context) {
        Timber.plant(ConsoleTree.builder()
                .debugColor(Color.parseColor("#42A5F5"))
                .infoColor(Color.parseColor("#54DA45"))
                .verboseColor(Color.parseColor("#43535A"))
                .assertColor(Color.parseColor("#D18CD1"))
                .warnColor(Color.parseColor("#E4E544"))
                .errorColor(Color.parseColor("#F06F85"))
                .build());
    }
}
