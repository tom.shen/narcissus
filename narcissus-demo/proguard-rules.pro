# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-ignorewarnings                     # 忽略警告，避免打包时某些警告出现
-optimizationpasses 5               # 指定代码的压缩级别
-dontusemixedcaseclassnames         # 是否使用大小写混合
-dontskipnonpubliclibraryclasses    # 是否混淆第三方jar
-dontpreverify                      # 混淆时是否做预校验
-verbose                            # 混淆时是否记录日志
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*        # 混淆时所采用的算法

# 保持Activity类及其子类不被混淆
-keep public class * extends android.app.Activity
# 保持Application类及其不被混淆
-keep public class * extends android.app.Application
# 保持Service类及其子类不被混淆
-keep public class * extends android.app.Service
# 保持BroadcastReceiver类及其不被混淆
-keep public class * extends android.content.BroadcastReceiver
# 保持ContentProvider类及其子类不被混淆
-keep public class * extends android.content.ContentProvider
# 保持BackupAgentHelper类及其子类不被混淆
-keep public class * extends android.app.backup.BackupAgentHelper
# 保持Preference类及其子类不被混淆
-keep public class * extends android.preference.Preference
# 保持Fragment类及其子类不被混淆
-keep public class * extends android.app.Fragment
#保持引用v4包下Fragment及其子类不被混淆
-keep public class * extends android.support.v4.app.Fragment
#保持 Serializable类及其实现类不被混淆
-keepnames class * implements java.io.Serializable
# 如果引用了v4或者v7包
-dontwarn android.support.**

# 保持 native 方法不被混淆
-keepclasseswithmembernames class * {
    native <methods>;
}
#保护注解
-keepattributes *Annotation*
-keep class * extends java.lang.annotation.Annotation {*;}
# 不混淆内部类
-keepattributes InnerClasses
#不混淆资源类即R文件
-keepclassmembers class **.R$* {
    public static <fields>;
}
#避免混淆泛型 如果混淆报错建议关掉
-keepattributes Signature
# 保持枚举 enum 类不被混淆
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
# 保持自定义控件类不被混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
# 保持自定义控件类不被混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
# 保持Activity中参数类型为View的所有方法
-keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
}
#所有View的子类及其子类的get、set方法都不进行混淆
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}
# 保持包含org.json.JSONObject参数的构造方法的类
-keepclassmembers class * {
    public <init> (org.json.JSONObject);
}