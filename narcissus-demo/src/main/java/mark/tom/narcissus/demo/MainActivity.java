package mark.tom.narcissus.demo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import mark.tom.narcissus.demo.databinding.ActivityMainBinding;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Timber.d("AComponents中执行 -> putString(\"AComponents\", \"Data from AComponents -> \" + new Random().nextInt(100))\n");
        Timber.d("MainActivity中执行 -> getString(\"AComponents\", \"\")\n");
        Timber.i("Result -> %s", PreferenceManager.getDefaultSharedPreferences(this)
                .getString("AComponents", ""));
    }
}
