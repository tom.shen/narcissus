package mark.tom.narcissus.demo;

import android.app.Application;

import androidx.annotation.NonNull;

import mark.tom.narcissus.Narcissus.NarcissusBuilder;
import mark.tom.narcissus.template.ILogWatcher;
import mark.tom.narcissus.template.IModuleFilter;

public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        NarcissusBuilder.get(this).with(new ILogWatcher() {
            @Override
            public boolean watch() {
                return BuildConfig.DEBUG;
            }
        }).with(new IModuleFilter() {
            @Override
            public boolean filter(@NonNull String moduleName) {
                return !moduleName.contains("Module");
            }
        }).build().init();
    }
}
