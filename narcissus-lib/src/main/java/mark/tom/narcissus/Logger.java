package mark.tom.narcissus;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mark.tom.narcissus.template.ILogWatcher;

class Logger {

    // 日志标记
    private final static String TAG = "Narcissus";
    // 日志开关
    private final ILogWatcher watcher;

    Logger(@Nullable ILogWatcher mWatcher) {
        this.watcher = mWatcher;
    }

    void d(@NonNull final String message) {
        log(message, Log.DEBUG);
    }

    void i(@NonNull final String message) {
        log(message, Log.INFO);
    }

    void e(@NonNull final String message) {
        log(message, Log.ERROR);
    }

    private void log(@NonNull final String message, int priority) {
        if (null == watcher || !watcher.watch()) {
            return;
        }
        Log.println(priority, TAG, message);
    }
}
