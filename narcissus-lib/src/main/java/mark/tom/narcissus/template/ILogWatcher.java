package mark.tom.narcissus.template;

/**
 * @Description: 日志记录器控制开关
 * @Author: Tom.Shen
 * @Package: mark.tom.narcissus.template
 * @Date: 2020/10/16
 * @Copyright: 版权归 Mark 所有
 */
public interface ILogWatcher {

    /**
     * @Description: 日志开关
     * @Author: Tom.Shen
     * @Date: 2020/10/16
     */
    boolean watch();
}
