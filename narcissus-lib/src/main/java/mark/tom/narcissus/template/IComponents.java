package mark.tom.narcissus.template;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * @Description: 自定义组件接口
 * @Author: Tom.Shen
 * @Package: mark.tom.narcissus.template
 * @Date: 2020/9/28
 * @Copyright: 版权归 Mark 所有
 */
public interface IComponents {

    /**
     * Description: 注册组件
     * Author:  Tom.Shen
     * Date: 2020/9/28 9:04
     */
    void registerComponents(@NonNull Context context);
}
