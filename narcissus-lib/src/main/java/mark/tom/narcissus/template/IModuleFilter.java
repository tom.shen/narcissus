package mark.tom.narcissus.template;

import androidx.annotation.NonNull;

/**
 * @Description: 组建过滤器
 * @Author: Tom.Shen
 * @Package: mark.tom.narcissus.template
 * @Date: 2020/10/19
 * @Copyright: 版权归 Mark 所有
 */
public interface IModuleFilter {

    /**
     * @Description: 过滤
     * @Author: Tom.Shen
     * @Date: 2020/10/19
     */
    boolean filter(@NonNull String moduleName);
}
