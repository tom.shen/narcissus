# 保持Narcissus组件的接口
-keep interface mark.tom.narcissus.template.*
# 保留Narcissus初始化启动类
-keep class mark.tom.narcissus.Narcissus {
    public *;
}
# 保留Narcissus构造类
-keep class mark.tom.narcissus.Narcissus$NarcissusBuilder {
    public *;
}
# 保留narcissus包下公用方法
-keepclassmembers class mark.tom.narcissus.** {
    public <methods>;
}
# 保持Narcissus组件接口的实现类
-keep class * implements mark.tom.narcissus.template.IComponents {
    public void *(android.content.Context);
}