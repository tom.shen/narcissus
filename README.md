# Narcissus

![license](http://img.shields.io/badge/license-Apache2.0-brightgreen.svg)
![Release Version](https://img.shields.io/badge/release-1.0.1.1-yellow.svg)

> 组件化工程 独立初始化工具

## **使用方法**

1. 在Application中进行初始化
   ```javascript
    NarcissusBuilder.get(this).with(new ILogWatcher() {
        @Override
        public boolean watch() {
            return BuildConfig.DEBUG;
        }
    }).with(new IModuleFilter() {
        @Override
        public boolean filter(@NonNull String moduleName) {
            return false;
        }
    }).build().init();
   ```

2. 在组件工程中新增IComponents 实现类
   ```java
   public class AComponents implements IComponents {
      @Override
      public void registerComponents(@NonNull Context context) {
         // 添加组件内初始化代码
      }
   }
   ```

3. 在组件工程的AndroidManifest文件中注册该类
   ```xml
   <application>
        <!-- 需要注册在application标签下 -->
        <meta-data
            android:name="AModule"
            android:value="mark.tom.narcissus.demo.AComponents"/>
    </application>
   ```

## **Proguard**

```javascript
-keep interface mark.tom.narcissus.template.IComponents
-keep class mark.tom.narcissus.Narcissus {
    public *;
}
-keep class mark.tom.narcissus.Narcissus$NarcissusBuilder {
    public *;
}
-keepclassmembers class mark.tom.narcissus.** {
    public <methods>;
}
-keep class * implements mark.tom.narcissus.template.IComponents {
    public void *(android.content.Context);
}
```

## **Gradle**

   ``` groovy
   api 'mark.tom:narcissus:1.0.1.1'
   ```

## **注意事项**

&ensp;&ensp;注册的事务还是会在application中执行 请不要将耗时的任务放在主线程中执行

## **LICENSE**

    Copyright 2020 Tom.Shen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.